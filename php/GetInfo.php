<?php

if ($argc == 1)
{
	echo "Gebruik: {$argv[0]} <kenteken>\n";
	exit(1);
}
$kenteken = $argv[1];

$soap = new SoapClient('http://appel.local:8080/thema42wo2opdr5/ThesaurusService?WSDL', 
	array('trace' => true, 'cache_wsdl' => WSDL_CACHE_NONE));
$params = new stdclass();
$params->kenteken = $kenteken;
try
{
	var_dump($soap->GetInfo($params));
	echo "Request:\n";
	echo formatXml($soap->__getLastRequest());
	echo "\nResponse:\n";
	echo formatXml($soap->__getLastResponse());
}
catch (Exception $e)
{
	echo "Exception caught (" . $e->getCode() . "): " . $e->getMessage() . "\n";
}

function formatXml($xml)
{
	$dom = new DOMDocument();
	$dom->formatOutput = true;
	$dom->loadXML($xml);
	return $dom->saveXML();
}
