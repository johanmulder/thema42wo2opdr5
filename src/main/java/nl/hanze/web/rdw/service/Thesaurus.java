package nl.hanze.web.rdw.service;

import java.util.HashMap;
import java.util.Map;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(serviceName = "ThesaurusService")
public class Thesaurus
{
	private final Map<String, Info> cars;

	public Thesaurus()
	{
		cars = new HashMap<>();
		// Throw in some "fake" cars.
		cars.put(
				"JZ-HV-21",
				getDummyInfo(123L, "456", "JZ-HV-21", "Blauw",
						getDummyModel("Rover", 2000, "620i", "L")));
		cars.put(
				"38-DH-NZ",
				getDummyInfo(123L, "789", "38-DH-NZ", "Grijs",
						getDummyModel("Volkswagen", 1900, "Bora", "D")));
		cars.put(
				"10-JS-BK",
				getDummyInfo(123L, "987", "10-JS-BK", "Blauw",
						getDummyModel("Volkswagen", 1900, "Passat", "D")));
	}

	private Info getDummyInfo(long bsn, String chassisNummer, String kenteken, String kleur,
			Model model)
	{
		Info info = new Info();
		info.setBsn(bsn);
		info.setChassisNummer(chassisNummer);
		info.setKenteken(kenteken);
		info.setKleur(kleur);
		info.setModel(model);
		info.setStatusInfoAanvraag("OK");
		return info;
	}

	private Model getDummyModel(String merkNaam, float motorInhoud, String typeAanduiding,
			String typeMotor)
	{
		Model model = new Model();
		model.setMerkNaam(merkNaam);
		model.setMotorInhoud(motorInhoud);
		model.setTypeAanduiding(typeAanduiding);
		model.setTypeMotor(typeMotor);
		return model;
	}

	@WebMethod
	public Info GetInfo(@WebParam(name = "kenteken") String kenteken)
	{
		if (cars.containsKey(kenteken))
			return cars.get(kenteken);
		Info info = new Info();
		info.setKenteken(kenteken);
		info.setStatusInfoAanvraag("NOT FOUND");
		return info;
	}
}
